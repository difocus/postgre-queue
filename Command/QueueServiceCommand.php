<?php

namespace ShopExpress\QueueBundle\Command;

use ShopExpress\CoreBundle\Doctrine\UuidGenerator;
use ShopExpress\QueueBundle\Event\AckQueueMessageEvent;
use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class QueueServiceCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('queue:service')
            ->setDescription('q testing');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queue = $this->getContainer()
            ->get('queue.service')
            ->getQueueManager('lk_video_list');

        for ($i= 0; $i < 5; $i++) {
            $queue->put(QueueMessage::createNew(rand(1, 10), array('test' => '123')));
        }

        $messages = $queue->find(array("test" => "123"));
        foreach ($messages as $message) {
            //var_dump($message->getId());
            $queue->del($message);
        }
    }
}
