<?php

namespace ShopExpress\QueueBundle\Storage;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use ShopExpress\QueueBundle\ValueObjects\QueueSettings;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;

class PostgresQueueLogStorage implements QueueLogStorageInterface
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var QueueSettings
     */
    private $queueSettings;

    private $collected = true;

    private $collectedLog = [];

    private $touchedStorage = false;

    public function __construct(Connection $connection, QueueSettings $queueSettings)
    {
        $this->connection = $connection;
        $this->collected = $queueSettings->isCollectedLog();
        $this->queueSettings = $queueSettings;
    }

    /**
     * @param string $action
     * @param QueueMessage $message
     * @throws DBALException
     */
    public function put($action, QueueMessage $message)
    {
        if ($this->collected) {
            $this->collectedLog[] = func_get_args();
        } else {
            $this->touchedStorage = true;
            $this->connection->executeUpdate(
                'INSERT INTO processing_queue_log 
          (queue_name, created_at, message, action) 
          VALUES (:queue_name, now(), :message, :action) ',
                array(
                    'action' => $action,
                    'queue_name' => $this->queueSettings->getName(),
                    'message' => $message->getLogJson()
                )
            );
        }
    }

    public function __destruct()
    {
        if (count($this->collectedLog)) {
            $this->collected = false;
            $this->connection->beginTransaction();
            try {
                foreach ($this->collectedLog as $item) {
                    call_user_func_array([$this, 'put'], $item);
                }
                $this->connection->commit();
            } catch (\Exception $e) {
                $this->connection->rollBack();
            }
        }

        //clean up
        if ($this->touchedStorage && $this->queueSettings->getDaysToStoreLog() > 0) {
            $interval = $this->queueSettings->getDaysToStoreLog() . " day";

            // не делаем чистку таким способом т.к. они устраивают гонку в базе
           /* $this->connection->executeUpdate("delete from processing_queue_log
where queue_name = :queue_name
and  created_at < (now() - interval '{$interval}')",
                array(
                    'queue_name' => $this->queueSettings->getName(),
                )
            );*/
        }
    }
}
