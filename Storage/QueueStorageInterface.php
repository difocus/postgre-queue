<?php

namespace ShopExpress\QueueBundle\Storage;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;

interface QueueStorageInterface
{
    public function get($limit = 1);
    public function put(QueueMessage $message);
    public function delete(QueueMessage $message);
    public function find(array $criteria);
}
