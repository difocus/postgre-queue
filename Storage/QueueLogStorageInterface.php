<?php

namespace ShopExpress\QueueBundle\Storage;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;

interface QueueLogStorageInterface
{
    public function put($action, QueueMessage $message);
}
