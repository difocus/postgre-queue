<?php

namespace ShopExpress\QueueBundle\Storage;

use ShopExpress\QueueBundle\ValueObjects\QueueSettings;
use Doctrine\DBAL\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class QueueStorageProvider
{
    /**
     * @var ContainerInterface
     */
    private $container;
    private $storage = array();
    private $log = array();

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param QueueSettings $queueSettings
     * @return mixed|null
     */
    public function getStorage(QueueSettings $queueSettings)
    {
        if (!isset($this->storage[$queueSettings->getName()])) {
            if ($queueSettings->getType() == 'postgres'
                && $connection = $this->getDoctrineConnection($queueSettings->getConnectionName())
            ) {
                $this->storage[$queueSettings->getName()] = new PostgresQueueStorage($connection, $queueSettings);
            }
        }

        return isset($this->storage[$queueSettings->getName()]) ? $this->storage[$queueSettings->getName()] : null;
    }

    /**
     * @param QueueSettings $queueSettings
     * @return mixed|null
     */
    public function getStorageLog(QueueSettings $queueSettings)
    {
        if (!isset($this->log[$queueSettings->getName()])) {
            if ($queueSettings->getType() == 'postgres'
                && $connection = $this->getDoctrineConnection($queueSettings->getConnectionName())
            ) {
                $this->log[$queueSettings->getName()] = new PostgresQueueLogStorage($connection, $queueSettings);
            }
        }

        return isset($this->log[$queueSettings->getName()]) ? $this->log[$queueSettings->getName()] : null;
    }

    /**
     * @param $name
     * @return Connection|null
     */
    private function getDoctrineConnection($name)
    {
        /** @var Connection[] $connections */
        $connections = $this->container->getParameter('doctrine.connections');

        if (isset($connections[$name])) {
            /** @var Connection $connection */
            $connection = $this->container->get($connections[$name]);
            //$connection->getConfiguration()->setSQLLogger(null);
            return $connection;
        }
    }
}
