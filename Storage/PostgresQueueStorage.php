<?php

namespace ShopExpress\QueueBundle\Storage;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use ShopExpress\QueueBundle\ValueObjects\QueueSettings;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;

class PostgresQueueStorage implements QueueStorageInterface
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var QueueSettings
     */
    private $queueSettings;

    public function __construct(Connection $connection, QueueSettings $queueSettings)
    {
        $this->connection = $connection;
        $this->queueSettings = $queueSettings;
    }

    /**
     * @param int $limit
     * @return array
     * @throws DBALException
     */
    public function get($limit = 1)
    {
        $result = array();
        $params = array(
            'queue_name' => $this->queueSettings->getName(),
            'limit' => $limit
        );

        $sql = "UPDATE processing_queue
SET locked_at = now() 
WHERE id IN (
        SELECT id FROM processing_queue
  WHERE (locked_at IS NULL OR locked_at < now() - INTERVAL '1 day') 
  AND queue_name = :queue_name 
  AND scheduled_at <= now()
  ORDER BY priority, scheduled_at
  LIMIT :limit
  FOR NO KEY UPDATE SKIP LOCKED
)
RETURNING *";

        $statement = $this->connection->executeQuery($sql, $params);
        while ($row = $statement->fetch()) {
            $result[] = $this->populate($row);
        }

        return $result;
    }

    /**
     * @param QueueMessage $message
     * @return int
     * @throws DBALException
     */
    public function put(QueueMessage $message)
    {
        $params = array(
            'id' => $message->getId(),
            'queue_name' => $this->queueSettings->getName(),
            'priority' => $message->getPriority(),
            'payload' => $message->getPayloadJson(),
            'locked_at' => null,
            'create_at' => $message->getCreatedAt() ? $message->getCreatedAt()->format('Y-m-d H:i:s') : null,
            'scheduled_at' => $message->getScheduledAt()->format('Y-m-d H:i:s')
        );

        if ($message->getId()) {
            return $this->connection->executeUpdate(
                "UPDATE processing_queue 
             SET priority = :priority, payload = :payload, locked_at = :locked_at, scheduled_at = :scheduled_at 
             WHERE id = :id",
                $params
            );
        } else {
            return $this->connection->executeUpdate(
                "
        INSERT INTO processing_queue (queue_name, priority, payload, locked_at, create_at, scheduled_at) 
        VALUES (:queue_name, :priority, :payload, :locked_at, now(), :scheduled_at)",
                $params
            );
        }
    }

    /**
     * @param QueueMessage $message
     * @return int
     * @throws DBALException
     */
    public function delete(QueueMessage $message)
    {
        if (!$message->getId()) {
            throw new ParameterNotFoundException('id is empty');
        }

        return $this->connection
            ->executeUpdate(
                'DELETE FROM processing_queue WHERE id = :id',
                array('id' => $message->getId())
            );
    }

    /**
     * @param array $row
     * @return QueueMessage
     */
    private function populate(array $row)
    {
        return new QueueMessage(
            $row['id'],
            $row['priority'],
            json_decode($row['payload'], true),
            new \DateTime($row['create_at']),
            new \DateTime($row['scheduled_at']),
            new \DateTime($row['locked_at'])
        );
    }

    /**
     * @param array $criteria
     * @return array
     * @throws DBALException
     */
    public function find(array $criteria)
    {
        $result = array();
        $params = array(
            'queue_name' => $this->queueSettings->getName(),
            'criteria' => json_encode($criteria)
        );

        $sql = "UPDATE processing_queue
SET locked_at = now() 
WHERE id IN (
        SELECT id FROM processing_queue
  WHERE queue_name = :queue_name
  AND payload @> CAST (:criteria AS jsonb)
  ORDER BY scheduled_at, priority
  FOR NO KEY UPDATE SKIP LOCKED
)
RETURNING *";

        $statement = $this->connection->executeQuery($sql, $params);
        while ($row = $statement->fetch()) {
            $result[] = $this->populate($row);
        }

        return $result;
    }
}
