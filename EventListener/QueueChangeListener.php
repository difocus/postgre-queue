<?php

namespace ShopExpress\QueueBundle\EventListener;

use ShopExpress\QueueBundle\Event\AckQueueMessageEvent;
use ShopExpress\QueueBundle\Event\DeferQueueMessageEvent;
use ShopExpress\QueueBundle\Event\DeleteQueueMessageEvent;
use ShopExpress\QueueBundle\Event\PutQueueMessageEvent;
use ShopExpress\QueueBundle\Service\QueueService;
use ShopExpress\QueueBundle\Storage\QueueStorageProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QueueChangeListener implements EventSubscriberInterface
{
    /**
     * @var QueueService
     */
    private $queueService;
    /**
     * @var QueueStorageProvider
     */
    private $storageProvider;

    public function __construct(QueueService $queueService, QueueStorageProvider $storageProvider)
    {
        $this->queueService = $queueService;
        $this->storageProvider = $storageProvider;
    }

    /**
     * @param AckQueueMessageEvent $event
     */
    public function onAsk(AckQueueMessageEvent $event)
    {
        $settings = $this->queueService->getSettingsByName($event->getQueueName());
        $this->storageProvider->getStorageLog($settings)->put(
            'ack',
            $event->getQueueMessage()
        );
    }

    /**
     * @param DeferQueueMessageEvent $event
     */
    public function onDefer(DeferQueueMessageEvent $event)
    {
        $settings = $this->queueService->getSettingsByName($event->getQueueName());
        $this->storageProvider->getStorageLog($settings)->put(
            'defer',
            $event->getQueueMessage()
        );
    }

    /**
     * @param DeleteQueueMessageEvent $event
     */
    public function onDelete(DeleteQueueMessageEvent $event)
    {
        $settings = $this->queueService->getSettingsByName($event->getQueueName());
        $this->storageProvider->getStorageLog($settings)->put(
            'delete',
            $event->getQueueMessage()
        );
    }

    /**
     * @param PutQueueMessageEvent $event
     */
    public function onPut(PutQueueMessageEvent $event)
    {
        $settings = $this->queueService->getSettingsByName($event->getQueueName());
        $this->storageProvider->getStorageLog($settings)->put(
            'put',
            $event->getQueueMessage()
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            AckQueueMessageEvent::$name => 'onAsk',
            DeferQueueMessageEvent::$name => 'onDefer',
            PutQueueMessageEvent::$name => 'onPut',
            DeleteQueueMessageEvent::$name => 'onDelete',
        );
    }
}
