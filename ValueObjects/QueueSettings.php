<?php

namespace ShopExpress\QueueBundle\ValueObjects;

class QueueSettings
{
    /**
     * @var array
     */
    private $settings;

    /**
     * QueueSettings constructor.
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return isset($this->settings['name']) ? $this->settings['name'] : '';
    }

    /**
     * @return mixed|string
     */
    public function getType()
    {
        return isset($this->settings['type']) ? $this->settings['type'] : '';
    }

    /**
     * @return mixed|string
     */
    public function getConnectionName()
    {
        return isset($this->settings['connection']) ? $this->settings['connection'] : '';
    }

    /**
     * @return bool|mixed
     */
    public function isCollectedLog()
    {
        return isset($this->settings['collectedLog']) ? $this->settings['collectedLog'] : true;
    }

    /**
     * @return int|mixed
     */
    public function getDaysToStoreLog()
    {
        return isset($this->settings['daysToStoreLog']) ? $this->settings['daysToStoreLog'] : 0;
    }
}
