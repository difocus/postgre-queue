<?php

namespace ShopExpress\QueueBundle\ValueObjects;

final class QueueMessage
{
    private $id;
    private $priority;
    private $payload;
    private $lockedAt;
    private $scheduledAt;
    private $createdAt;

    public function __construct(
        $id,
        $priority,
        array $payload,
        \DateTime $createdAt,
        \DateTime $scheduledAt,
        \DateTime $lockedAt = null
    ) {
        $this->id = $id;
        $this->priority = $priority;
        $this->payload = $payload;
        $this->scheduledAt = $scheduledAt;
        $this->createdAt = $createdAt;
        $this->lockedAt = $lockedAt;
    }

    /**
     * @param $priority
     * @param $payload
     * @return QueueMessage
     */
    public static function createNew($priority, $payload)
    {
        return new self(null, $priority, $payload, new \DateTime(), new \DateTime());
    }

    /**
     * Новое сообщение для переноса обработки
     * @param int|\DateTime $hourOrTime смещение от текущего времени в часах либо точное время
     * @return QueueMessage
     */
    public function deferMessage($hourOrTime)
    {
        return new self(
            $this->getId(),
            $this->getPriority(),
            $this->getPayload(),
            $this->getCreatedAt(),
            $hourOrTime instanceof \DateTime
                ? $hourOrTime
                : new \DateTime('+ ' . $hourOrTime . ' hours'),
            null
        );
    }

    /**
     * Новое сообщение для разблокировки сообщения в очереди
     * @return QueueMessage
     */
    public function unlockMessage()
    {
        return new self(
            $this->getId(),
            $this->getPriority(),
            $this->getPayload(),
            $this->getCreatedAt(),
            $this->getScheduledAt(),
            null
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return string
     */
    public function getPayloadJson()
    {
        return json_encode($this->payload);
    }

    /**
     * @return \DateTime
     */
    public function getScheduledAt()
    {
        return $this->scheduledAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getLockedAt()
    {
        return $this->lockedAt;
    }

    /**
     * @return string
     */
    public function getLogJson()
    {
        return json_encode(array(
            'id' => $this->getId(),
            'priority' => $this->getPriority(),
            'payload' => $this->getPayload(),
            'created_at' => $this->getCreatedAt()->format('Y-m-d H:i:s'),
            'scheduled_at' => $this->getScheduledAt()->format('Y-m-d H:i:s'),
            'locked_at' => $this->getLockedAt() ? $this->getLockedAt()->format('Y-m-d H:i:s') : null
        ));
    }
}
