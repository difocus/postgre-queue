<?php

namespace ShopExpress\QueueBundle\Event;

class PutQueueMessageEvent extends QueueMessageEvent
{
    public static $name = __CLASS__;
}
