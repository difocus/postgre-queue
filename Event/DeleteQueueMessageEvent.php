<?php

namespace ShopExpress\QueueBundle\Event;

class DeleteQueueMessageEvent extends QueueMessageEvent
{
    public static $name = __CLASS__;
}
