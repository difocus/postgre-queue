<?php

namespace ShopExpress\QueueBundle\Event;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use Symfony\Component\EventDispatcher\Event;

class AckQueueMessageEvent extends QueueMessageEvent
{
    public static $name = __CLASS__;
}
