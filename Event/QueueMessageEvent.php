<?php

namespace ShopExpress\QueueBundle\Event;

use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use Symfony\Component\EventDispatcher\Event;

abstract class QueueMessageEvent extends Event
{
    /**
     * @var QueueMessage
     */
    private $queueMessage;
    private $queueName;

    public function __construct($queueName, QueueMessage $queueMessage)
    {
        $this->queueMessage = $queueMessage;
        $this->queueName = $queueName;
    }

    /**
     * @return QueueMessage
     */
    public function getQueueMessage()
    {
        return $this->queueMessage;
    }

    /**
     * @return mixed
     */
    public function getQueueName()
    {
        return $this->queueName;
    }
}
