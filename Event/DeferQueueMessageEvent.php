<?php

namespace ShopExpress\QueueBundle\Event;

class DeferQueueMessageEvent extends QueueMessageEvent
{
    public static $name = __CLASS__;
}
