<?php

namespace ShopExpress\QueueBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('queue');

        $rootNode
            ->isRequired()
            ->children()
                ->arrayNode('queues')
                    ->useAttributeAsKey('key')
                    ->isRequired()
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->isRequired()->end()
                            ->scalarNode('type')->isRequired()->end()
                            ->scalarNode('connection')->isRequired()->end()
                            ->booleanNode('collectedLog')->defaultTrue()->end()
                            ->integerNode('daysToStoreLog')->defaultValue(90)->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
