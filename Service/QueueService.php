<?php

namespace ShopExpress\QueueBundle\Service;

use ShopExpress\QueueBundle\Storage\QueueStorageProvider;
use ShopExpress\QueueBundle\ValueObjects\QueueSettings;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Сервис для работы с очередями
 */
final class QueueService
{
    /**
     * @var QueueStorageProvider
     */
    private $storageProvider;
    private $queueSettings = array();
    private $queueManagers = array();
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        QueueStorageProvider $storageProvider,
        EventDispatcherInterface $eventDispatcher,
        $settings
    ) {
        $this->storageProvider = $storageProvider;
        $this->eventDispatcher = $eventDispatcher;
        foreach ($settings['queues'] as $queueSettings) {
            $settings = new QueueSettings($queueSettings);
            $this->queueSettings[$settings->getName()] = $settings;
        }
    }

    /**
     * Получить менеджера обменя конкретной очереди
     * @param $queueName
     * @return QueueManager
     */
    public function getQueueManager($queueName)
    {
        if (!isset($this->queueManagers[$queueName])) {
            $settings = $this->getSettingsByName($queueName);
            $storage = $this->storageProvider->getStorage($settings);
            $this->queueManagers[$queueName] = new QueueManager($storage, $settings, $this->eventDispatcher);
        }

        return $this->queueManagers[$queueName];
    }

    /**
     * @param $queueName
     * @return QueueSettings
     */
    public function getSettingsByName($queueName)
    {
        if (isset($this->queueSettings[$queueName])) {
            return $this->queueSettings[$queueName];
        } else {
            throw new ParameterNotFoundException(sprintf('queue "%s" not found in settings', $queueName));
        }
    }
}
