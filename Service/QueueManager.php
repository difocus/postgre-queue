<?php

namespace ShopExpress\QueueBundle\Service;

use ShopExpress\QueueBundle\Event\AckQueueMessageEvent;
use ShopExpress\QueueBundle\Event\DeferQueueMessageEvent;
use ShopExpress\QueueBundle\Event\DeleteQueueMessageEvent;
use ShopExpress\QueueBundle\Event\PutQueueMessageEvent;
use ShopExpress\QueueBundle\Storage\QueueStorageInterface;
use ShopExpress\QueueBundle\ValueObjects\QueueMessage;
use ShopExpress\QueueBundle\ValueObjects\QueueSettings;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Менеджер для работы с конкретной очередью
 */
final class QueueManager
{
    /**
     * @var QueueStorageInterface
     */
    private $queueStorage;
    /**
     * @var QueueSettings
     */
    private $queueSettings;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        QueueStorageInterface $queueStorage,
        QueueSettings $queueSettings,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->queueStorage = $queueStorage;
        $this->queueSettings = $queueSettings;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Взять из очереди
     *
     * @param  int $limit
     * @return QueueMessage[]
     */
    public function take($limit = 1)
    {
        return $this->queueStorage->get($limit);
    }

    /**
     * Положить в очередь
     * @param $message
     */
    public function put(QueueMessage $message)
    {
        if ($result = $this->queueStorage->put($message)) {
            $event = new PutQueueMessageEvent($this->queueSettings->getName(), $message);
            $this->eventDispatcher->dispatch(get_class($event), $event);
        }

        return $result;
    }

    /**
     * Сообщить об удачной обработке
     * @param $message
     */
    public function ack(QueueMessage $message)
    {
        if ($result = $this->queueStorage->delete($message)) {
            $event = new AckQueueMessageEvent($this->queueSettings->getName(), $message);
            $this->eventDispatcher->dispatch(get_class($event), $event);
        }

        return $result;
    }

    /**
     * @param QueueMessage $message
     * @return mixed
     */
    public function unlock(QueueMessage $message)
    {
        $message = $message->unlockMessage();
        return $this->queueStorage->put($message);
    }

    /**
     * Перенести обработку сообщения
     * @param QueueMessage $message
     * @param int|\DateTime $hourOrTime  смещение от текущего времения в часах либо точное время
     */
    public function defer(QueueMessage $message, $hourOrTime = 1)
    {
        $message = $message->deferMessage($hourOrTime);

        if ($result = $this->queueStorage->put($message)) {
            $event = new DeferQueueMessageEvent($this->queueSettings->getName(), $message);
            $this->eventDispatcher->dispatch(get_class($event), $event);
        }

        return $result;
    }

    /**
     * Удалить сообщение из очереди
     * @param QueueMessage $message
     */
    public function del(QueueMessage $message)
    {
        if ($result = $this->queueStorage->delete($message)) {
            $event = new DeleteQueueMessageEvent($this->queueSettings->getName(), $message);
            $this->eventDispatcher->dispatch(get_class($event), $event);
        }

        return $result;
    }

    /**
     * Поиск в сообщениях по параметрам
     * @param array $criteria
     * @return QueueMessage[]
     */
    public function find(array $criteria)
    {
        return $this->queueStorage->find($criteria);
    }
}
